﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bum : MonoBehaviour
{
    public static int EXPLOSION_TIME = 30;
    public static int FALLOUT_TIME = 10;
    private bool isEffective;
    void Start()
    {
        isEffective = true;
        StartCoroutine("BumBum");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player" && isEffective)
        {
            GameMenager.menager.HitPlayer(2);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player" && isEffective)
        {
            isEffective = false;
            GameMenager.menager.HitPlayer(2);
        }
    }

    private IEnumerator BumBum()
    {
        for(int i=0; i<EXPLOSION_TIME; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        isEffective = false;
        for (int i = 0; i < FALLOUT_TIME; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);

    }
}
