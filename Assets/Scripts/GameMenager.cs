﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenager : MonoBehaviour
{
    public static int LEVELS_TO_BEAT = 6;
    private int beatedLevels = 0;
    private int beatedSimpleLevels = 0;
    private int beatedHardLevels = 0;
    private int died = 0;

    public static GameMenager menager;
    private CameraControler mainCamera;
    private PlayerControler player;
    private UIMenager ui;
    private SoundEffects sfx;

    private int score;
    private DifficultyLevel currentDifficulty;
    private int currentMultiplayer;
    private int lastMultiplayer;
    private GameObject levelToLoad;
    private GameObject currentLevel;
    public bool isActive = true;


    [SerializeField] private GameObject introLevel;
    [SerializeField] private GameObject[] normalLevels;
    public static int NORMAL_MULTIPLAYER = 1; 
    [SerializeField] private GameObject[] hardLevels;
    public static int HARD_MULTIPLAYER = 3;

    private void Awake()
    {
        menager = this;
        currentDifficulty = DifficultyLevel.insane;
        currentMultiplayer = 1;
        score = 0;
    }

    

    
    public void RegisterPlayer(PlayerControler playerControler)
    {
        player = playerControler;
    }

    public void RegisterCamera(CameraControler cameraControler)
    {
        mainCamera = cameraControler;
    }

    public void RegisterUi(UIMenager menager)
    {
        ui = menager;
    }

    public void RegisterSFX(SoundEffects sfx)
    {
        this.sfx = sfx;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Init");
        LoadLevel(introLevel);
    }

    private IEnumerator Init()
    {
        yield return new WaitForFixedUpdate();
        mainCamera.Follow(player.transform, false, 0, true, -2.5f);
    }

    public void HitPlayer(int demage)
    {
        int hp = player.HP();
        player.TakeDemage(demage);
        if (hp != player.HP())
        {
            mainCamera.Shake();
            ui.SetHP(player.HP());
        }
    }

    public void PlayerDied()
    {
        isActive = false;
        mainCamera.Shake();
        player.Die();
        sfx.PlayGameOverSound();
        ui.EnableRestartMenu();
        StartCoroutine("ListenForRestart");
    }

    private IEnumerator ListenForRestart()
    {
        bool restarted = false;
        while (!restarted)
        {
            if (Input.GetKey(KeyCode.R))
            {
                restarted = true;
                RestartLevel();
            }
            yield return new WaitForFixedUpdate();
        }
    }

    private void RestartLevel()
    {
        StartCoroutine("Resurect");
    }

    public IEnumerator Resurect()
    {
        if (currentLevel != null) { Destroy(currentLevel); }
        score -= 15000 * currentMultiplayer;
        died += 1;
        if (score < 0) { score = 0; }
        ui.UpdateScore(score);
        GameObject go = Instantiate(levelToLoad, new Vector3(0, 0, 0), transform.rotation);
        currentLevel = go;
        yield return new WaitForFixedUpdate();
        player.TeleportTo(GameObject.FindGameObjectWithTag("Spawn").transform.position);
        yield return new WaitForFixedUpdate();
        ui.DisablbeRestartMenu();
        player.Resurect();
        ui.SetHP(player.HP());
        isActive = true;
    }

    public void NextLevel(DifficultyLevel difficulty)
    {
        score += 1000 * currentMultiplayer;
        score += player.HP() * 1500 * currentMultiplayer;
        ui.UpdateScore(score);
        if (currentDifficulty == DifficultyLevel.normal)
        {
            beatedSimpleLevels += 1;
        }
        else if(currentDifficulty == DifficultyLevel.hard)
        {
            beatedHardLevels += 1;
        }
        beatedLevels += 1;
        if (beatedLevels < LEVELS_TO_BEAT)
        {
            player.ResettHP();
            ui.SetHP(player.HP());
            sfx.PlayTeleportSound();
            float rand = Random.Range(0, 10);
            if (difficulty == DifficultyLevel.normal)
            {
                if (beatedSimpleLevels < normalLevels.Length)
                {
                    currentDifficulty = DifficultyLevel.normal;
                    currentMultiplayer = NORMAL_MULTIPLAYER;
                    levelToLoad = normalLevels[beatedSimpleLevels];
                }
                else
                {
                    currentDifficulty = DifficultyLevel.hard;
                    currentMultiplayer = HARD_MULTIPLAYER;
                    levelToLoad = hardLevels[beatedHardLevels];
                }
            }
            else if (difficulty == DifficultyLevel.hard)
            {
                if (beatedHardLevels < hardLevels.Length)
                {
                    currentDifficulty = DifficultyLevel.hard;
                    currentMultiplayer = HARD_MULTIPLAYER;
                    levelToLoad = hardLevels[beatedHardLevels];
                }
                else
                {
                    currentDifficulty = DifficultyLevel.normal;
                    currentMultiplayer = NORMAL_MULTIPLAYER;
                    levelToLoad = normalLevels[beatedSimpleLevels];
                }
            }
            LoadLevel(levelToLoad);
        }
        else
        {
            player.Die();
            ui.ShowVictoryScreen(died, score, beatedHardLevels, beatedSimpleLevels);
        }
    }

    private void LoadLevel(GameObject level)
    {
        levelToLoad = level;
        StartCoroutine("Load");
    }

    private IEnumerator Load()
    {
        if (currentLevel != null) { Destroy(currentLevel); }
        GameObject go = Instantiate(levelToLoad, new Vector3(0, 0, 0), transform.rotation);
        currentLevel = go;
        yield return new WaitForFixedUpdate();
        player.TeleportTo(GameObject.FindGameObjectWithTag("Spawn").transform.position);
        isActive = true;
    }

    public void CollectCoin()
    {
        score += 400 * currentMultiplayer;
        ui.UpdateScore(score);
    }

    


}
