﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    private float offsetX, offsetY;
    private bool followX, followY;
    private Transform followTarget;

    private void Start()
    {
        if (GameMenager.menager != null)
        {
            GameMenager.menager.RegisterCamera(this);
        }
    }

    public void Follow(Transform target, bool followInX = true, float x = 0, bool followInY = true , float y = 0)
    {
        offsetX = x;
        offsetY = y;
        followX = followInX;
        followY = followInY;
        followTarget = target;
        StartCoroutine("FollowMe");
    }

    private IEnumerator FollowMe()
    {
        while (true)
        {
            if (followX)
            {
                transform.position = new Vector3(followTarget.position.x + offsetX, transform.position.y, -10);
            }
            if (followY)
            {
                transform.position = new Vector3(transform.position.x, followTarget.position.y + offsetY,-10);
            }
            
            yield return new WaitForFixedUpdate();
        }
    }

    public void Shake()
    {
        StopAllCoroutines();
        StartCoroutine("ShakeIt");
    }

    public IEnumerator ShakeIt()
    {
        float BASIC_SHAKE_DISTANCE = 0.3f;
        int NUMBER_OF_SHAKES = 6;
        float SHAKE_SPEED = 1f;
        float MARGIN_OF_ERROR = 0.001f;
        Vector3 origin = transform.position;
        for (int i = 0; i < NUMBER_OF_SHAKES; i++)
        {
            Vector3 destination = new Vector3(Random.Range(-BASIC_SHAKE_DISTANCE, BASIC_SHAKE_DISTANCE) + transform.position.x, Random.Range(-BASIC_SHAKE_DISTANCE, BASIC_SHAKE_DISTANCE)+ transform.position.y, transform.position.z);
            while (Vector3.Distance(transform.position, destination) > MARGIN_OF_ERROR)
            {
                transform.position = Vector3.Lerp(transform.position, destination, SHAKE_SPEED);
                yield return new WaitForFixedUpdate();
            }
            transform.position = destination;
            yield return new WaitForFixedUpdate();
            while (Vector3.Distance(transform.position, origin) > MARGIN_OF_ERROR)
            {
                transform.position = Vector3.Lerp(transform.position, origin, SHAKE_SPEED);
                yield return new WaitForFixedUpdate();
            }
            transform.position = origin;
            yield return new WaitForFixedUpdate();
        }
        StartCoroutine("GoBack");
    }

    public IEnumerator GoBack()
    {
        float MARGIN_OF_ERROR = 0.0001f;
        float CAMERA_SPEED = 0.1f;
        if(followX && followY)
        {
            while(Vector3.Magnitude(transform.position - new Vector3(followTarget.position.x + offsetX, followTarget.position.y + offsetY, -10)) > MARGIN_OF_ERROR)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(followTarget.position.x + offsetX, followTarget.position.y + offsetY, -10), CAMERA_SPEED);
                yield return new WaitForFixedUpdate();
            }
        }
        else if (followX)
        {
            while (Vector3.Magnitude(transform.position - new Vector3(followTarget.position.x + offsetX, transform.position.y, -10)) > MARGIN_OF_ERROR)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(followTarget.position.x + offsetX, transform.position.y, -10), CAMERA_SPEED);
                yield return new WaitForFixedUpdate();
            }
        }
        else if(followY)
        {
            while (Vector3.Magnitude(transform.position - new Vector3(transform.position.x, followTarget.position.y + offsetY, -10)) > MARGIN_OF_ERROR)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, followTarget.position.y + offsetY, -10), CAMERA_SPEED);
                yield return new WaitForFixedUpdate();
            }
        }
        StartCoroutine("FollowMe");
    }
}
