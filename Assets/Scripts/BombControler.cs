﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombControler : MonoBehaviour
{
    private SpriteRenderer image;
    private AudioSource audio;
    [SerializeField] AudioClip beep;
    public GameObject bum;
    public static int NUMBER_OF_TICKS = 3;
    public static int TICK_TIMER = 20;
    private bool triggered;

    void Start()
    {
        triggered = false;
        audio = transform.GetComponent<AudioSource>();
        image = transform.GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && GameMenager.menager.isActive && !triggered)
        {
            triggered = true;
            StartCoroutine("Tick");
        }
    }

    private IEnumerator Tick()
    {
        for(int i=0; i < NUMBER_OF_TICKS; i++)
        {
            audio.PlayOneShot(beep);
            for(int j=0; j < TICK_TIMER/2; j++)
            {
                image.color = new Color(image.color.r, image.color.g - 2.0f / (float)(TICK_TIMER), image.color.b - 2.0f / (float)(TICK_TIMER));
                yield return new WaitForFixedUpdate();
            }
            for (int j = 0; j < TICK_TIMER/2; j++)
            {
                image.color = new Color(image.color.r, image.color.g + 2.0f / (float)(TICK_TIMER), image.color.b + 2.0f / (float)(TICK_TIMER));
                yield return new WaitForFixedUpdate();
            }
        }
        Instantiate(bum, transform.position, transform.rotation);
        yield return new WaitForFixedUpdate();
        Destroy(gameObject);
    }
}
