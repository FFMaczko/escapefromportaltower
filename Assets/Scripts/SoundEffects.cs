﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    private AudioSource audio;
    [SerializeField] private AudioClip teleport;
    [SerializeField] private AudioClip gameOver;
    // Start is called before the first frame update
    void Start()
    {
        audio = transform.GetComponent<AudioSource>();
        GameMenager.menager.RegisterSFX(this);
    }

    public void PlayTeleportSound()
    {
        audio.PlayOneShot(teleport);
    }

    public void PlayGameOverSound()
    {
        audio.PlayOneShot(gameOver);
    }
}
