﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public static int TIME_TO_COLLECT = 20;
    public static float COLLECT_SPEED = 0.11f;
    private bool colected;
    private SpriteRenderer sprite;
    private AudioSource audio;
    [SerializeField] private AudioClip pickUp;

    private void Start()
    {
        sprite = transform.GetComponent<SpriteRenderer>();
        audio = transform.GetComponent<AudioSource>();
        colected = false;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !colected && GameMenager.menager.isActive)
        {
            GameMenager.menager.CollectCoin();
            colected = true;
            StartCoroutine("Collect");
        }
    }

    private IEnumerator Collect()
    {
        audio.PlayOneShot(pickUp);
        for(int i = 0; i < TIME_TO_COLLECT; i++)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y+COLLECT_SPEED, 0);
            sprite.color = new Color(1, 1, 1, sprite.color.a - 1 / (float)(TIME_TO_COLLECT));
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
    }


}
